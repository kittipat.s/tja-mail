datastoreClass = (server, options) ->

#  https://cloud.google.com/nodejs/docs/reference/datastore/1.4.x/Datastore#delete

  datastoreAdmin = server.app.datastoreAdmin

  server.app.datastore =
    class datastore

      getPath: () -> datastoreAdmin.key(@parent.concat([@kind, @id]))
      @parentAndKind: (parentAndKind) ->
        elements = parentAndKind.split('/')
        kind: elements.pop(), parent: elements
      @path: (path) ->
        elements = path.split('/')
        id: elements.pop(), kind: elements.pop(), parent: elements

      keys: (object=@data) ->

        recur = (object, parent, array) -> for key, value of object
          if object not instanceof Array
            key = parent + '.' + key
          else key = parent
          if value instanceof Array then key = key + '[]'
          array.push key
          if value instanceof Object then recur value, key, array

        array = []

        for key, value of object
          if value instanceof Array then key = key + '[]'
          array.push key
          if value instanceof Object then recur value, key, array

        array

      excludeIndexes: ->
        @excludeFromIndexes = @excludeFromIndexes ? @keys @data

        @indexes = @indexes ? if server.app.indexes[@kind]? then server.app.indexes[@kind].slice(0) else []

        if @indexes?

          indexes = @indexes

          for index in @indexes
            splits = index.split "."
            tmp = ''
            if splits.length > 1 then for split in splits
              tmp = tmp + split
              indexes.push tmp
              tmp = tmp + '.'

          indexes = indexes.filter (value, index, self) -> self.indexOf(value) == index

          @indexes = indexes

          @excludeFromIndexes = @excludeFromIndexes.filter (index) -> indexes.indexOf(index) < 0

        @excludeFromIndexes = @excludeFromIndexes.filter (value, index, self) -> self.indexOf(value) == index

      constructor: (@data, @kind, @id=server.app.id(), @parent='', @excludeFromIndexes=null, @indexes=null) ->
        kind: @kind, id: @id, parent: @parent, data: @data, excludeFromIndexes: @excludeFromIndexes, indexes: @indexes

      save: ->
#        @data.updated = new Date()
        datastoreAdmin.upsert { key: @getPath(), data: @data, excludeFromIndexes: @excludeIndexes()}
          .then ((data) -> @status = data[0]; @).bind(@)

      load: ->
        datastoreAdmin.get @getPath()
          .then ((data) -> @data = data[0]; @).bind(@)

      delete: ->
        datastoreAdmin.delete @getPath()

      isExist: -> @data?

      update: ->
        datastoreAdmin.get @getPath()
        .then ((data) ->
#          delete @data.created if @data.created?
          @data = Object.assign(data[0], @data) if data[0]?
#          @data.updated = new Date()
          datastoreAdmin.upsert { key: @getPath(), data: @data, excludeFromIndexes: @excludeIndexes()}
            .then ((data) -> @status = data[0]; @).bind(@)
        ).bind(@)

      updateWithoutUpdated: ->
        datastoreAdmin.get @getPath()
        .then ((data) ->
#          delete @data.created if @data.created?
          @data = Object.assign(data[0], @data) if data[0]?
          datastoreAdmin.upsert { key: @getPath(), data: @data, excludeFromIndexes: @excludeIndexes()}
            .then ((data) -> @status = data[0]; @).bind(@)
        ).bind(@)

#     https://cloud.google.com/datastore/docs/concepts/transactions
      @transaction: -> datastoreAdmin.transaction()
      saveWithTransaction: (transaction) ->
#        @data.updated = new Date()
        transaction.save { key: @getPath(), data: @data, excludeFromIndexes: @excludeIndexes()}

      loadWithTransaction: (transaction) ->
        transaction.get @getPath()
          .then ((data) -> @data = data[0]; @).bind(@)



      @initFromNewData: (data, parentAndKind, id=server.app.id()) ->
        element = datastore.parentAndKind(parentAndKind)
#        data.created = new Date()
        new datastore(data, element.kind, id, element.parent)

      @initFromID: (parentAndKind, id) ->
        element = datastore.parentAndKind(parentAndKind)
        new datastore({}, element.kind, id, element.parent)

      @initFromPath: (path) ->
        element = datastore.path(path)
        new datastore({}, element.kind, element.id, element.parent)

      @initFromNewDataAndPath: (data, path) ->
        element = datastore.path(path)
        new datastore(data, element.kind, element.id, element.parent)

      @initFromEntity: (entity) ->
        KEY = entity[datastoreAdmin.KEY]
        datastore.initFromNewDataAndPath(entity, KEY.path.join('/'))

      @getFromID: (parentAndKind, id, selects) ->
        obj = datastore.initFromID(parentAndKind, id)
        obj.load()
        .then (datum) ->
          obj = {}
          if datum.data?
            if selects? then obj[select] = datum.data[select] for select in selects
            else obj = datum.data
          data: Object.assign(obj, id: datum.id)
        .catch (error) ->
          console.error error.message
          throw error

      @getCollection: (parentAndKind, limit=null, queries=null, pageCursor=null, orders=null, selects=null) ->
        parentAndKind = datastore.parentAndKind parentAndKind
        query = datastoreAdmin.createQuery parentAndKind.kind
        query = query.start server.app.decrypt(pageCursor) if pageCursor?
        query = query.hasAncestor datastoreAdmin.key parentAndKind.parent if parentAndKind.parent.length
        query = query.filter q[0],q[1],q[2] for q in queries if queries?
        if orders? then for o in orders #[['created', descending: true]], [['created']]
          if o.length == 2 then query = query.order o[0], o[1] else query = query.order o[0]
        query = query.limit limit if limit?
        datastore.runQuery(query)

      @getCollectionWithAutocomplete: (parentAndKind, limit=null, queries=null, pageCursor=null, orders=null, selects=null) ->
        datastore.getCollection(parentAndKind, limit, queries, pageCursor, orders, selects)
        .then (result) ->
          data = result.data.map (datum) ->
            obj = {}
            if selects? then obj[select] = datum.data[select] for select in selects
            else obj = datum.data
            Object.assign(obj, id: datum.id)
          data: data, pageCursor: result.info.pageCursor, moreResults: result.info.moreResults
        .catch (error) ->
          console.error error.message
          throw error

      @runQuery: (query) ->
        datastoreAdmin.runQuery(query)
          .then (result) ->
            entities = result[0]
            info = result[1]
            output = entities.map (entity) -> datastore.initFromEntity entity
            data: output, info: pageCursor: server.app.encrypt(info.endCursor), moreResults: info.moreResults != "NO_MORE_RESULTS"


exports.plugin = {
  name: "datastore"
  register: datastoreClass
  dependencies: ["encrypt", "idGenerator"]
  once: true
}
