generate = require('nanoid/generate')
alphabet = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ'

idClass = (server, options) ->

  server.app.id = (length=32) -> generate(alphabet, length)
  server.app.shortID = (length=4) -> generate(alphabet, length)

exports.plugin = {
  name: "idGenerator"
  register: idClass
  once: true
}