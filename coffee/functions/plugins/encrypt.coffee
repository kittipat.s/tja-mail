encryptClass = (server, options) ->

  crypto = require 'crypto'
  algorithm = 'aes-256-ctr'
  password = "Edvisory*1"

  server.app.encrypt = (text) ->
    cipher = crypto.createCipher algorithm, password
    crypted = cipher.update text, 'utf8', 'hex'
    crypted += cipher.final('hex')

  server.app.decrypt = (text) ->
    try
      decipher = crypto.createDecipher algorithm, password
      dec = decipher.update text, 'hex', 'utf8'
      dec += decipher.final('utf8')
    catch
      undefined

exports.plugin = {
  name: "encrypt"
  register: encryptClass
  once: true
}
