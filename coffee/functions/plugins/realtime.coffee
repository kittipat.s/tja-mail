realtimeClass = (server, options) ->

#  https://cloud.google.com/nodejs/docs/reference/datastore/1.4.x/Datastore#delete

  admin = server.app.admin
  db = admin.database()

  server.app.realtime =
    class realtime

      constructor: (@data, @ref) -> {ref: @ref, data: @data}

      save: -> db.ref(@ref).set(@data)
      delete: -> db.ref(@ref).set(null)
      update: -> db.ref(@ref).update(@data)
      
      incrementLogic = (currentValue) -> (currentValue || 0) + 1
      decrementLogic = (currentValue) ->
        if currentValue > 0 then currentValue - 1
        else if currentValue == 0 then 0
        else null

      #     func must always handle null or return currentValue

#      transaction: (func) -> db.ref(@ref).transaction(func).then ((result) ->
#        if result.committed then @data=result.snapshot.val()
#        else throw "Nothing committed!"
#      ).bind(@)

      transactionOnComplete = (error, committed, snapshot) ->
#        if error then console.log 'Transaction failed abnormally!', error
#        else if not committed then console.log 'We aborted the transaction.'
#        else console.log 'Transaction run successfully.'
#        console.log 'Data changed to: ', snapshot.val()

      transaction: (func) ->
        db.ref(@ref)
          .transaction(func, transactionOnComplete, false)
          .then ((input) ->
            if input.committed then @data=input.snapshot.val()
            else throw "Nothing committed!"
          ).bind(@)

#      https://firebase.google.com/docs/reference/node/firebase.database.Reference#transaction

      decrement: -> @transaction(decrementLogic)
      increment: -> @transaction(incrementLogic)

      load: -> db.ref(@ref).once('value').then ((data) -> @data = data.val(); @).bind(@)

      @initFromID: (path, id) -> @initFromRef path+'/'+id
      @initFromRef: (ref=null) -> new @({}, ref)
      @initFromPath: (ref=null) -> new @({}, ref)
      @initFromNewData: (data, ref) -> new @(data, ref)

exports.plugin = {
  name: "realtime"
  register: realtimeClass
  dependencies: ["idGenerator"]
  once: true
}

