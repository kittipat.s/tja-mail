module.exports = (server) ->

  datastore = server.app.datastore
  realtime = server.app.realtime
  file = server.app.file
  boom = require 'boom'
  joi = require 'joi'

  server.route [
    {
      method: 'GET'
      path: '/cronemail/{day}/{state}'
      config:
        cors: origin: ['*']
      handler: (request, reply) ->
        try
          today = new Date()
          numberOfDay = parseInt(request.params.day)
          events = await datastore.getCollection("event", null, [
            ["rundate", "<=", new Date(today.getFullYear(),today.getMonth(),today.getDate()+numberOfDay)],
            ["rundate", ">=", new Date(today.getFullYear(),today.getMonth(),today.getDate()+(numberOfDay-1))]
          ])

          done = 0

          sendMailGroup = (event, number=15) -> new Promise (resolve) ->
            loop
              result = await datastore.getCollection("application", number, [["event","=", event.id], ["state", "=", request.params.state]], pageCursor)
              await Promise.all result.data.map (application) -> sendEmailSuccess(event, application)
              done += result.data.length
              pageCursor = result.info.pageCursor
              break unless result.info.moreResults
            resolve(done)

          for event in events.data
            await sendMailGroup(event)

          return "Done "+done+", Event "+events.data.length
        catch e
          return boom.badImplementation(e)
    }
  ]


  sendEmailSuccess = (event, application) ->
    recipient = application.data.email
    event_title = (event.data.title.th ? event.data.title)
    payment_id = application.data.orderid
    application_id = application.id

    if application.data.type == '0'
      name = application.data.profile.firstName + " " + application.data.profile.lastName

    subject = "Thaijogging.org: ยินดีด้วย คุณได้เป็นนักวิ่งงาน " + event_title

    if application.data.type == '0'
      html = "Thaijogging.org: ยินดีด้วย " + name + " ได้เป็นนักวิ่งงาน " + event_title + " อย่างเป็นทางการแล้ว<br><br>" + "ระบบได้บันทึกการชำระและการลงทะเบียนเป็นที่เรียบร้อยและสมบูรณ์<br><br>"
    else
      html = "Thaijogging.org: ยินดีด้วย<br><br>" + "ระบบได้บันทึกการชำระและการลงทะเบียนเป็นที่เรียบร้อยและสมบูรณ์<br><br>"

    shirts = {}
    raceTypes = {}
    for shirt in event.data.shirt
      shirts[shirt.id] = {}
      shirts[shirt.id].name = shirt.name.th ? shirt.name
      shirts[shirt.id].size = {}
      for size in shirt.size
        shirts[shirt.id].size[size.id] = size.name

    for raceType in event.data.raceType
      raceTypes[raceType.id] = raceType.name.th ? raceType.name

    html += "<p>รายละเอียดการสมัคร</p><br>"
    html += "เลขที่ใบสมัคร: " + application_id + "<br>"
    html += "เลขที่ใบชำระเงิน: " + payment_id + "<br>"
    html += "งานวิ่ง: "+(event.data.title.th ? event.data.title)+"<br>"
    deliverMethods = {
      "0": "รับด้วยตนเอง"
      "1": "จัดส่งทางไปรษณีย์"
    }
    html += "วิธีการจัดส่งเสื้อ: "+deliverMethods[application.data.deliverMethod]+"<br>"
    if application.data.deliverMethod == "1"
      html += "ข้อมูลที่อยู่สำหรับจัดส่งเสื้อ: "+
        application.data.address.addressDetail+" "+
        "แขวง/ตำบล"+application.data.address.subdistrict+" "+
        "เขต/อำเภอ"+application.data.address.district+" "+
        "จังหวัด"+application.data.address.province+" "+
        application.data.address.zipcode+" "+
        ""+"<br>"

    html += "<br>รายละเอียดผู้สมัคร<br>"

    switch application.data.type
      when "0"
        html += "ชื่อผู้สมัคร: "+application.data.profile.firstName+" "+application.data.profile.lastName+"<br>"
        html += "อีเมล์: "+application.data.email+"<br>"
        application.data.raceType = raceTypes[application.data.raceType]
        for shirt in application.data.shirt
          shirt.size = shirts[shirt.type].size[shirt.size]
          shirt.type = shirts[shirt.type].name
          html += "ชนิดเสื้อ: "+shirt.type+"<br>"
          html += "ขนาดเสื้อ: "+shirt.size+"<br>"
          html += "<br>"
      else
        for runner in application.data.runners
          html += "ชื่อผู้สมัคร: "+runner.profile.firstName+" "+runner.profile.lastName+"<br>"
          html += "อีเมล์: "+(runner.profile.email ? "")+"<br>"
          runner.raceType = raceTypes[runner.raceType]
          for shirt in runner.shirt
            shirt.size = shirts[shirt.type].size[shirt.size]
            shirt.type = shirts[shirt.type].name
            html += "ชนิดเสื้อ: "+shirt.type+"<br>"
            html += "ขนาดเสื้อ: "+shirt.size+"<br>"
            html += "<br>"

    html += "<br><img src=\"cid:image.png\"/><br><br>"

    html += "ขอให้เตรียมตัวให้ดีต่อการวิ่งและพักผ่อนให้เพียงพอ<br><br>สามารถติดตามเคล็ดลับต่างๆและข่าวสารได้ที่ <a href=\"https://www.facebook.com/สมาพันธ์ชมรมเดินวิ่ง-เพื่อสุขภาพไทย-1566165170310323/\">Facebook สมาพันธ์ชมรมเดิน-วิ่งเพื่อสุขภาพไทย</a>"

    qr = require('qr-image')

    attachments = [{
      content: qr.imageSync(application_id, { type: 'png' , ec_level: 'H', size: 6})
      cid: 'image.png'
    }]

    nodemailer = require 'nodemailer'

    aws = require 'aws-sdk'
    aws.config.loadFromPath('./configs/aws.json')

    transporter = nodemailer.createTransport SES: new aws.SES apiVersion: '2012-10-17'

    mailOptions = {
      from: 'noreply@thaijogging.org',
      to: recipient
      subject: subject
      html: html
      attachments: attachments
    }

    transporter.sendMail(mailOptions)