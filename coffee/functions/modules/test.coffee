module.exports = (server) ->

  datastore = server.app.datastore
  realtime = server.app.realtime

  server.route [
    {
      method: 'GET'
      path: '/0000'
      config:
        cors: origin: ['*']
      handler: (request, h) ->
        x = await datastore.getCollectionWithAutocomplete("application", 1)
        return statusCode: 200, data: x
    }
    {
      method: 'GET'
      path: '/0001'
      config:
        cors: origin: ['*']
      handler: (request, h) ->
        x = await realtime.initFromRef('/users').load()
        return x
    }
  ]