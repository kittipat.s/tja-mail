Hapi = require('@hapi/hapi')
server = Hapi.server()
functions = require('firebase-functions')
admin = require('firebase-admin')
server.app.admin = admin.initializeApp()

{Datastore} = require '@google-cloud/datastore'
server.app.datastoreAdmin = new Datastore()
server.app.indexes = {}

loaded = false

makeReady = ->

  if not loaded

    loaded = true

    glob = require('glob')

    await server.register [
      require './plugins/encrypt'
      require './plugins/datastore'
      require './plugins/id'
      require './plugins/realtime'
    ]

    #    Import modules
    for path in glob.sync('**/*.js', {cwd: __dirname+'/modules'})
      (require('./modules/'+path) server)


exports.email = functions.region('asia-northeast1').runWith(memory: '128MB').https.onRequest (request, response) ->

  delete request.headers["accept-encoding"]

  options =
    method: request.method
    url: request.path
    payload: if request.rawBody then request.rawBody else request
    headers: request.headers
    validate: true

  await makeReady()

  try
    res = await server.inject options
    response.set(key, value) for key, value of res.headers
    response.status(res.statusCode).send(res.result)

  catch err
    response.send err

exports.scheduledEmail = functions.region('asia-northeast1').pubsub.schedule("0 19 * * *").timeZone("Asia/Bangkok").onRun (context) ->

  options =
    method: "GET"
    url: "/cronemail/6/1"
    validate: true

  await makeReady()

  try
    res = await server.inject options
    console.log res.result

  catch err
    console.log err
